import seaborn as sns
import matplotlib.pyplot as plt
import plotly.graph_objs as go
from plotly.offline import plot


def build_layout(x_column, y_column, title):
    layout = go.Layout(
        title=go.layout.Title(
            text=title,
            font=dict(
                family='Courier New, monospace',
                size=30,
                color='#7f7f7f'
            )
        ),
        xaxis=go.layout.XAxis(
            title=go.layout.xaxis.Title(
                text=x_column,
                font=dict(
                    family='Courier New, monospace',
                    size=18,
                    color='#7f7f7f'
                )
            )
        ),
        yaxis=go.layout.YAxis(
            title=go.layout.yaxis.Title(
                text=y_column,
                font=dict(
                    family='Courier New, monospace',
                    size=18,
                    color='#7f7f7f'
                )
            )
        )
    )
    return layout


def profiling(df):
    profile = df.profile_report(title='Pandas Profiling Report')
    profile.to_file(output_file="profiling.html")


def histogram(df, x_column, y_column='Número de eventos', title='Histograma', filename='histogram.html'):
    data = [go.Histogram(x=df[x_column])]
    layout = build_layout(x_column, y_column, title)
    fig = go.Figure(data=data, layout=layout)
    plot(fig, filename=filename)


def boxplot(df, x_column, y_column, title='Boxplot Único', filename='boxplot.html'):
    data = [go.Box(x=df[x_column], y=df[y_column])]
    layout = build_layout(x_column, y_column, title)
    fig = go.Figure(data=data, layout=layout)
    plot(fig, filename=filename)


def boxplot_multiple(df, categories_vector, x_column, y_column='Número de eventos', title='Boxplot multiplo',
                     filename='boxplot_multiple.html'):
    data = []
    for i in categories_vector:
        df_categories_vector = df[df[i] == True]
        data.append(go.Box(name=i, y=df_categories_vector[y_column]))
    layout = build_layout(x_column, y_column, title)
    fig = go.Figure(data=data, layout=layout)
    plot(fig, filename=filename)


def correlation(df, method="spearman", title='Correlação', filename='correlation.png'):
    corr = df.corr(method=method)
    plt.figure(figsize=(12, 12))
    ax = sns.heatmap(
        corr,
        vmin=-1, vmax=1, center=0,
        cmap='coolwarm',
        square=True
    )
    ax.axes.set_title(title, fontsize=30, alpha=0.5)
    ax.get_figure().savefig(filename)


def radar(df, categories_vector, x_column, title='Grafico de radar', filename='radar_plot.html'):
    mean_rule_value = []
    for i in categories_vector:
        mean_rule_value.append(df[df[i] == True][x_column].mean())
    data = [go.Scatterpolar(
        r=mean_rule_value,
        theta=categories_vector,
        fill='toself'
    )]
    layout = build_layout(x_column, 'Categorias do Radar', title=title)
    fig = go.Figure(data=data, layout=layout)
    fig.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=True
            ),
        ),
        showlegend=False
    )
    plot(fig, filename=filename)


def radar_passing_mean(df, mean_rule_value, x_column, title='Grafico de radar', filename='radar_plot.html'):
    data = [go.Scatterpolar(
        r=mean_rule_value,
        theta=x_column,
        fill='toself'
    )]
    layout = build_layout(x_column, 'Categorias do Radar', title=title)
    fig = go.Figure(data=data, layout=layout)
    fig.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=True
            ),
        ),
        showlegend=False
    )
    plot(fig, filename=filename)


def bar(df, categories_vector, title='Grafico de Barra utilizando frequência', filename='bar_frequency.html'):
    counts = []
    for i in categories_vector:
        counts.append(len(df[df[i] == True][i]) / len(df))
    data = [go.Bar(x=categories_vector, y=counts)]
    layout = build_layout('Categorias', 'Frequência', title)
    fig = go.Figure(data=data, layout=layout)
    plot(fig, filename=filename)